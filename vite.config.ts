import react from '@vitejs/plugin-react';
import {defineConfig} from 'vite';
import eslintPlugin from 'vite-plugin-eslint';
import dts from 'vite-plugin-dts';
import * as path from 'path';

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [
    {
      ...eslintPlugin({include: 'src/**/*.+(js|jsx|ts|tsx)'}),
      enforce: 'pre',
    },
    react(),
    dts({
      insertTypesEntry: true,
    }),
  ],
  build: {
    sourcemap: true,
    lib: {
      entry: path.resolve(__dirname, 'src/lib/index.ts'),
      name: 'MyLib',
      formats: ['es', 'umd'],
      fileName: (format) => `my-lib.${format}.js`,
    },
    rollupOptions: {
      external: ['react', 'react-dom', 'styled-components'],
      output: {
        globals: {
          react: 'React',
          'react-dom': 'ReactDOM',
          'styled-components': 'styled',
        },
      },
    },
  },
  server: {
    watch: {
      ignored: ['!**/.idea/**'],
    },
  },
});
